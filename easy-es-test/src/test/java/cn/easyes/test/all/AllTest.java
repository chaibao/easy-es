package cn.easyes.test.all;

import cn.easyes.test.TestEasyEsApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 全部测试-处手动挡索引相关API
 * <p>
 * Copyright © 2022 xpc1024 All Rights Reserved
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestEasyEsApplication.class)
public class AllTest {
    // 1.新增
    @Test
    public void testInsert(){

    }

    @Test
    public void testBatchInsert(){

    }

    // 2.修改
    @Test
    public void testUpdateById(){

    }

    @Test
    public void testUpdateByWrapper(){

    }

    @Test
    public void testUpdateByWrapperAndEntity(){

    }

    // 3.查询
    @Test
    public void testSelectOne(){

    }

    @Test
    public void testSelectList(){

    }

    @Test
    public void testConditionAllEq(){

    }

    @Test
    public void testConditionEq(){

    }

    @Test
    public void testConditionNe(){

    }

    @Test
    public void testConditionGt(){

    }

    // 4.删除
    @Test
    public void testDeleteByIdGe(){

    }

    @Test
    public void testConditionLt(){

    }

    @Test
    public void testConditionLe(){

    }

    @Test
    public void testConditionBetween(){

    }

    @Test
    public void testConditionNotBetween(){

    }

    @Test
    public void testConditionLike(){

    }

    @Test
    public void testConditionNotLike(){

    }

    @Test
    public void testConditionLikeLeft(){

    }

    @Test
    public void testConditionLikeRight(){

    }

    @Test
    public void testConditionIsNull(){

    }

    @Test
    public void testConditionIsNotNull(){

    }

    @Test
    public void testConditionIn(){

    }

    @Test
    public void testConditionNotIn(){

    }

    @Test
    public void testConditionGroupBy(){

    }

    @Test
    public void testConditionMax(){

    }

    @Test
    public void testConditionMin(){

    }

    @Test
    public void testConditionSum(){

    }


    @Test
    public void testConditionAvg(){

    }


    @Test
    public void testConditionDistinct(){

    }

    @Test
    public void testConditionLimit(){

    }

    @Test
    public void testConditionFrom(){

    }

    @Test
    public void testConditionSize(){

    }

    @Test
    public void testConditionIndex(){

    }

    @Test
    public void testConditionEnableMust2Filter(){

    }

    @Test
    public void testConditionAnd(){

    }

    @Test
    public void testConditionOrInner(){

    }

    @Test
    public void testConditionOrOuter(){

    }

    @Test
    public void testMixQuery(){

    }

    @Test
    public void testPageQuery(){

    }

    @Test
    public void testGetSource(){

    }

    @Test
    public void testFilterField(){

    }

    @Test
    public void testNotFilterField(){

    }

    @Test
    public void testOrderByDesc(){

    }

    @Test
    public void testOrderByAsc(){

    }

    @Test
    public void testConditionOrderByDesc(){

    }

    @Test
    public void testOrderByScore(){

    }

    @Test
    public void testSort(){

    }

    @Test
    public void testMatch(){

    }

    @Test
    public void testNotMatch(){

    }

    @Test
    public void testMatchPhrase(){
    }

    @Test
    public void testMatchAllQuery(){

    }

    @Test
    public void testMatchPhrasePrefixQuery(){

    }

    @Test
    public void testMultiMatchQuery(){

    }

    @Test
    public void testQueryStringQuery(){

    }

    @Test
    public void testPrefixQuery(){

    }

    @Test
    public void testWeight(){

    }

    @Test
    public void testHighLight(){

    }

    @Test
    public void testGeoBoundingBox(){

    }

    @Test
    public void testNotInGeoBoundingBox(){

    }

    @Test
    public void testGeoDistance(){

    }

    @Test
    public void testGeoPolygon(){

    }

    @Test
    public void testGeoShape(){

    }

    @Test
    public void testDeleteByWrapper(){

    }
}
